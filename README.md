## Cordova Beacon Plugin

### Installation

```
cordova plugin add https://gitlab.com/booth-guru/cordova-beacon.git
```

### Usage

##### Available Features for Android and iOS
 * Ranging
 * Monitoring
 * Request Location permission 
 * Request to use Bluetooth (iOS only) (Work automatically no code required)
 * Open phone settings if bluetooth or Location is turned off

Declare following on top to use plugin related methods
```
 declare let cordova: any;
```

##### Ask Location Permission 

Location Access permission is required for beacon ranging. You have to request permissions from the user of your app explicitly. You can do this through the plugin's API. See the LocationManager's related methods: requestWhenInUseAuthorization and requestAlwaysAuthorization for further details.

```
cordova.plugins.locationManager.requestAlwaysAuthorization(); 
		// or cordova.plugins.locationManager.requestWhenInUseAuthorization();
```

##### Start monitoring
```
		const delegate = new cordova.plugins.locationManager.Delegate();

		//Create Beacon object
		var uuid = '00000000-0000-0000-0000-000000000000';
		var identifier = 'beacon';
		var minor = 101;
		var major = 102;
		var beaconToRange = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid, major, minor);

		// Called when monitoring and the state of a region changes.
		delegate.didDetermineStateForRegion = function (pluginResult) {
		  if (pluginResult.state === 'CLRegionStateInside') {
		    console.log('Beacons is Inside the Region.');
		  } else if (pluginResult.state === 'CLRegionStateOutside') {
		    console.log('Beacons is Outside the Region.');
		  }
		};

		delegate.didStartMonitoringForRegion = function (pluginResult) {
		   console.log('didStartMonitoringForRegion:', JSON.stringify(pluginResult));
		};

		cordova.plugins.locationManager.setDelegate(delegate);

		cordova.plugins.locationManager.startMonitoringForRegion(beaconToRange);
		.fail(function(e) { console.error(e); })
			.done();
```
	
##### Stop monitoring
```
		var uuid = '00000000-0000-0000-0000-000000000000';
		var identifier = 'beacon';
		var minor = 101;
		var major = 102;
		var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid, major, minor);

		cordova.plugins.locationManager.stopMonitoringForRegion(beaconRegion)
			.fail(function(e) { console.error(e); })
			.done();
```



##### Start ranging
```
	delegate.didRangeBeaconsInRegion = function (pluginResult) {
		  console.log('pluginResult didRangeBeaconsInRegion: ',JSON.stringify(pluginResult));
		};

		var uuid = '00000000-0000-0000-0000-000000000000';
		var identifier = 'beacon';
		var minor = 101;
		var major = 102;
		var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid, major, minor);

		cordova.plugins.locationManager.startRangingBeaconsInRegion(beaconRegion);
		.fail(function(e) { console.error(e); })
			.done();
```

		
##### Stop ranging
```
		var uuid = '00000000-0000-0000-0000-000000000000';
		var identifier = 'beacon';
		var minor = 101;
		var major = 102;
		var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid, major, minor);

		cordova.plugins.locationManager.stopRangingBeaconsInRegion(beaconRegion)
		.fail(function(e) { console.error(e); })
		.done();
```

##### Bluetooth permission check
```
		cordova.plugins.locationManager.isBluetoothEnabled()
    	.then(function(isEnabled){
        console.log("isEnabled: " + isEnabled);
	        if (isEnabled) {
	            cordova.plugins.locationManager.disableBluetooth(); (Android only)
	        } else {
	            cordova.plugins.locationManager.enableBluetooth(); (Android only)
	        }
    	})
    	.fail(function(e) { console.error(e); })
    	.done();
```